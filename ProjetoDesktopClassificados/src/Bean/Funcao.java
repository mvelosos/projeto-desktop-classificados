/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author aluno
 */
public class Funcao {
    
    private int idFuncao;
    private String nomeFuncao;
    private String prerequisito;

    public Funcao() {
    }

    public Funcao(int idFuncao, String nomeFuncao, String prerequisito) {
        this.idFuncao = idFuncao;
        this.nomeFuncao = nomeFuncao;
        this.prerequisito = prerequisito;
    }

    public int getIdFuncao() {
        return idFuncao;
    }

    public void setIdFuncao(int idFuncao) {
        this.idFuncao = idFuncao;
    }

    public String getNomeFuncao() {
        return nomeFuncao;
    }

    public void setNomeFuncao(String nomeFuncao) {
        this.nomeFuncao = nomeFuncao;
    }

    public String getPrerequisito() {
        return prerequisito;
    }

    public void setPrerequisito(String prerequisito) {
        this.prerequisito = prerequisito;
    }
    
    
    
}
