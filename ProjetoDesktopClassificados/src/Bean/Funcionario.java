/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Bean;

import java.util.Date;

/**
 *
 * @author ricardoricrob
 */
public class Funcionario {
    
    private int matricula;
    private String nome;
    private Date dtAdmissao;
    private float salario;
    private int funcao;

    public Funcionario() {
    }

    public Funcionario(int matricula, String nome, Date dtAdmissao, float salario, int funcao) {
        this.matricula = matricula;
        this.nome = nome;
        this.dtAdmissao = dtAdmissao;
        this.salario = salario;
        this.funcao = funcao;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDtAdmissao() {
        return dtAdmissao;
    }

    public void setDtAdmissao(Date dtAdmissao) {
        this.dtAdmissao = dtAdmissao;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public int getFuncao() {
        return funcao;
    }

    public void setFuncao(int funcao) {
        this.funcao = funcao;
    }

    

}
